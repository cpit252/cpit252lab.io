---
title: "Project"
date: 2024-09-28T19:08:30+03:00
draft: false
---

## Final Project Description


The final project is the climax of this course. It gives you an opportunity to showcase what you have learned in this course and develop an application that utilizes object-oriented design patterns and code quality. You will build a tool or a software system in a team of up to three people. You should submit a project proposal for review by the end of week 2. Your project proposal should clearly state the problem you're trying to solve, the platform you will be developing for (Android, iOs, web, desktop), and the technology stack you plan to use. Your project should make use of at least THREE design patterns. Upon completion of your project, you will write a report describing the patterns used to solve any design problems you've encountered, and present your work to the class.

In short, come up with an interesting problem and solve it using design patterns you have learned in this course. You may extend a tool you prototyped for a previous class but have not actually fully implemented it as long as the remaining work is significantly more than the previously-submitted work. What to build for your project is entirely up to you. Here are just some of the possibilities. If you're not sure about an idea, talk to your instructor! And if you’d like to solicit collaborators for an idea you have, please post in MS Teams!

  - a command-line program using Java
  - a desktop application in Java Swing or JavaFX
  - a web-based application using JavaScript and PHP
  - an Android app using Java or Flutter
  - a web server using Python

It is recommended that you come up with an idea for a small yet interesting app that solves a problem you are passionate about. For example, if you're a car enthusiast, you may build a car repair estimate app, an app that lists the most common car problems and recalls for a given manufacturer and model, or an app that lists the resale value of a used car. And if you're into sport, you may build an app that shows all local sport events taken place in your city. The data can be manually populated into a database or collected via a public API. It does not have to be massive or complex! 

It's always great to start brainstorming with your team around something you all care about and love to build for. If you want to talk more about it or need help coming up with an idea, talk to your instructor.


### Grading

The project is worth 20% of your course grade. The following factors will be taken into consideration when grading your project:

- Depth of the problem.
- Soundness or correctness of the used design patterns.
- Code Quality as opposed to the number of lines of code submitted.
- Project management and team collaboration.

### What to submit for the proposal?

- List of team members and the title of the project. 
  - This project can be done in a group of up to three people (more people requires more work that corresponds to the depth of the problem).
- Description of the problem you're going to solve.
- Summary of anticipated deliverables (e.g., binary/executable file, jar file, source code)
- Architectural diagrams, UML diagrams, etc. [Optional]


> **Note:** If you plan to find a dataset for your app or integrate an API in your system, please take a look API Marketplaces or API discovery websites such as [RapidAPI], which may help you find a free API related to your problem. If you're having trouble coming up with an idea, talk to me here in MS Teams.


## Next Steps
Once your project proposal is accepted, you should get started and kick off your project. You're asked to start working on the project by creating a public repository on GitHub, add your team members as collaborators, create a project, add items to your project board, and push your code to GitHub **regularly** while working on the project.

Here's a summary of what you need to do:
1. [Accept the group project on GitHub Classroom](https://classroom.github.com/a/Jn5KwGHY)
1. Create a Team and add members by their GitHub username.
3. Create a repository with the name of your project.
4. [Create a project under the repository of your project](https://docs.github.com/en/issues/planning-and-tracking-with-projects/creating-projects/creating-a-project).
5. Create a board with three items: Todo, Doing, Done. Then add items to the Todo column. 
6. Assign items to team members and start collaborating on the project.