---
title: "Syllabus"
date: 2022-01-21T22:01:42+03:00
toc: false
draft: false
---

# CPIT-252 Software Design Patterns

# Course Syllabus

**Credits:** 3 credit hours
**Prerequisite:** CPIT-251

## Description and Objectives
The objective of this course is to study the principles behind the patterns of software and to then apply a number of basic patterns. This course covers fundamental aspects of large scale software architecture, defined frameworks, design patterns, and ways of developing and establishing systems based on components. The purpose of this course is: (1) to know the classical styles of software pattern and the need for a language to describe the architecture, (2) to understand how to express the qualities we want our architecture to provide to the system or systems we are building from it, and (3) to know how to achieve software qualities using TACTICS. Topics include envisioning architecture (architecture business cycle), architectural patterns, reference models, reference architectures, understanding quality attributes, achieving qualities using tactics, and how to document software architecture.



## Course Learning Outcomes (CLOs):

By completion of the course the students should be able to
1. Generate code from class diagrams and vice versa (2)
2. Identify any GoF creational design patterns for code or
scenario or design (2)
3. Design and implement GoF creational design patterns (2) 
4. Identify any GoF structural design patterns for code or
scenario or design (2)
5. Design and implement GoF structural design patterns (2) 
6. Identify any GoF behavioural design patterns for code or
scenario or design (2)
7. Design and implement GoF behavioural design patterns
(2)
8. Desing and Implement an application that simulate a
medium size project as part of a group (6)
9. Detect the appropriate design patterns in class diagrams
(2)
10. Recognize the design patterns from code (2)
11. Produce a pattern based design that maps the
requirements of a given scenario using design patterns
(6)
12. Implement code that maps the requirements of a given scenario or designs (6)
13. Patterns of Patterns: Compound Patterns (6)
14. Model View Controller Pattern (6)


## Textbook
- Design Patterns: Elements of Reusable Object-Oriented Software By Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides, Addison-Wesley; 2 edition (2003-09), ISBN-10: 0582844428, ISBN-13: 978-0582844421

## Additional Resources

1. The Head First Design Patterns textbook has a much more accessible set of code examples description/motivation of the patterns. We will use some of their examples in this course.
2. The [Refactoring.guru](https://refactoring.guru/design-patterns/catalog) design patterns pages is a good on-line reference with additional examples.

## Grading
- 20% Lab (10% Activities + 10% lab exam)
- 20% Project
- 30% Midterm exam 
- 30% Final exam

## Topic Coverage Durations

| Topic                          |  Weeks |
| ------------------------------ | -------- |
|  Introduction and an overview of design pattens                 | 1        |
|  Creational pattens           | 2        |
|  Structural pattens           | 2        |
|  Behavioral pattens           | 3        |
|  Coumpound pattens            | 1        |
|  Model View Controller (MVC)        | 1        |

 
