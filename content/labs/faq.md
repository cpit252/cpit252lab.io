---
title: "Labs, Git, GitHub, and GitHub classroom FAQ"
date: 2023-09-16T17:12:11+03:00
tod: true
draft: false
---

## What are Git, GitHub, and GitHub classroom?
Git is a free and open source distributed version control system designed to track and manage changes to software code over time.

GitHub is a code hosting platform for version control and collaboration.

GitHub classroom is a tool that lets educators create and manage classroom assignments and submissions using GitHub.

## I'm new GitHub where can I find help and resources on campus?
Every semester, we run a workshop on GitHub for introductory Software Engineering courses. You should attend a workshop or watch a previous one here:
- [Fall 2023 [TBA]: Introduction to Git and GitHub](#)
- [Winter 2023: Introduction to Git and GitHub](https://www.youtube.com/watch?v=DOKytu5AK7U)


## I keep getting authentication failed errors as the ones listed below. What should I do?

> "remote: Invalid username or password. fatal: Authentication failed"?

> "remote: Support for password authentication was removed on August 13, 2021. ... fatal: Authentication failed.""

Whenever you run the commands push/pull/fetch/clone to a remote repository on GitHub, git needs to authenticate with GitHub. By default, git will ask for your username and password. You can't use your GitHub password to authenticate git as GitHub will prevent you from doing so for security reasons. 
You need to generate a personal access token (think an application specific password for git only) as described below.

##### Generate a new personal access token

1. Log onto your Github.com, click on your profile picture, and ![click on settings](https://github.com/settings/profile).

2. On the settings page, click on "Developer Settings".

![Developer Settings](/images/labs/faq/gh-dev-settings.png)

3. Click on Personal access tokens (classic)

![Personal access tokens (classic)](/images/labs/faq/gh-tokens-classic.png)

4. Click on **Generate new token** -> **Generate new token (classic)**
![Generate new token](/images/labs/faq/gh-generate-new-token.png)

5. Select an expiration date and the repo scope. Scroll to the bottom and click on **Generate token**

![
New personal access token (classic)](/images/labs/faq/gh-new-personal-access-token-scopes.png)

6. Copy the generated personal access code.
![
Copy the generated token](/images/labs/faq/gh-generated-token.png)

7. Run any `git` command that requires authentication and paste the personal access token when prompted to enter your password.
  ```
  git fetch origin main
  Username for 'https://github.com': YOUR_USER_NAME
  Password for 'https://username@github.com': PASTE_ACCESS_TOKEN_HERE
  ``` 
8. Caching your GitHub credentials in Git
- If you're on Windows and have [Git for Windows](https://gitforwindows.org/) installed, then your personal access token should be stored using Git Credential Manager (GCM). The next time you clone an HTTPS URL that requires authentication, Git will prompt you to log in using a browser window, and GCM will store it locally on your device. For more information, see [Caching your GitHub credentials in Git](https://docs.github.com/en/get-started/getting-started-with-git/caching-your-github-credentials-in-git?platform=windows).
 - If you're on macOS, you should add the personal access token to your keychain. For more information, [please refer to this article from the GitHub documentation.](https://docs.github.com/en/get-started/getting-started-with-git/updating-credentials-from-the-macos-keychain#updating-your-credentials-via-keychain-access).


### If I have a question on a lab or ran into a problem with using GitHub classroom, whom should I ask?
You should reach out to your lab instructor.
