---
title: "Lab 9: The Observer Design Pattern"
date: 2022-04-10T01:32:11+03:00
draft: false
---

> The goal of this lab is to use the observer design pattern to solve a real-world problem.


Design patterns are proven solutions to solve recurring design problems to design flexible and reusable object-oriented software; that is, objects that are easier to implement, change, test and reuse. Behavioral pat­terns deal with the communication between objects and structuring the responsibilities between objects to keep­ them extensible, flex­i­ble and efficient.

The observer design pattern (a.k.a. dependency broadcasting, publish-subscribe pattern) is one of the twenty-three well-known Gang of Four (GoF) design patterns. It is classified under the category of behavioral patterns.
The observer design pattern allows us to implement a subscription service in which an object called the **subject** maintains a list of dependent objects called **observers**.  The subject notifies the observers of a change to the subject's state and broadcasts the change to them by calling the observers' methods.
In the observer pattern, a subject maintains a list of observers and notifies them of any state changes, without the need for the subject to keep track of the observers.

In this lab, we will work on the problem of creating a notification system in which a message needs to be broadcasted to multiple subscribers.

## Video

{{< youtube id="Kcc5_5HfpoA?rel=0" title="Lab 9: The observer design pattern" >}}

## Objectives
In this lab you will
1. understand a real-world scenario and choose when to apply the appropriate design pattern.
2. design and implement the observer design pattern.

### Requirement and Tools

- Java [JDK 1.8 or above.](https://www.oracle.com/java/technologies/downloads/)
- [Playwright](https://playwright.dev/) Playwright is a library that provides a powerful and flexible API for automating web browsers.
- [The JavaMail API](https://javaee.github.io/javamail/) is a library for building mail and messaging applications.
- An IDE (e.g., [Apache NetBeans](https://netbeans.apache.org/), [Eclipse](https://www.eclipse.org/) or [IntelliJ IDEA](https://www.jetbrains.com/idea/)).
- If you do not like to use an IDE, you may use any text editor (e.g., [VS Code](https://code.visualstudio.com/), [jEdit](http://www.jedit.org/), etc.) and the _Javac_ compiler.
- [Apache Maven](https://maven.apache.org/) is a build automation tool to build projects and manage their dependencies.




### Getting Started
If your instructor is using GitHub classroom, you will need to accept the assignment using [the link below](#deliverables-and-submission), clone the template repository, and import it as a project into your IDE.

If your instructor is not using GitHub classroom, clone and import the template project at [https://github.com/cpit252/lab-09 ↗](https://github.com/cpit252/lab-09).



### Problem Statement
A developer is working on an app that monitors the price of a product on an online store. The app allows users to receive a notification when the price of a given product drops. These notifications can be received via Email, Facebook, Whatsapp, SMS, etc. He wanted to support multiple notification services in the future. Thus, his design goal is to build up a system that is extensible and efficient where he can add a new notification service (e.g, push notification, Voice calls, etc.) without making additional changes to existing working services.

Below is the [web page](/html/lab-9/) he wants to regularly check the price on:

<iframe width="80%" height="450px" style="border:1px solid black" src="https://cpit252.gitlab.io/html/lab-9/"></iframe>


### The Subjects
__Subject.java__
{{< highlight Java "linenos=table" >}}
public interface Subject{
    public void subscribe(Observer o);
    public void unsubscribe(Observer o);
    public void notifyUpdate(String m);
}
{{< / highlight >}}

__Subject.java__
{{< highlight Java "linenos=table" >}}
public class MessageSubject implements Subject {
     
 
}
{{< / highlight >}}

### The Observers

__Observer.java__

{{< highlight Java "linenos=table" >}}

public abstract class Observer {
    private String recipient;

    public String getRecipient() {
        
    }

    public void setRecipient(String recipient) {

    }

    public abstract void update(String m);
}

{{< / highlight >}}


__FacebookObserver.java__
{{< highlight Java "linenos=table" >}}
public class FacebookObserver extends Observer{
}
{{< / highlight >}}

__WhatsappObserver.java__
{{< highlight Java "linenos=table" >}}
public class WhatsappObserver extends Observer{

}
{{< / highlight >}}

__EmailObserver.java__
{{< highlight Java "linenos=table" >}}
public class EmailObserver extends Observer{

}
{{< / highlight >}}


__App.java__

{{< highlight Java "linenos=table" >}}

public class App {

    private static String searchForProduct(String keyword){
        try (Playwright playwright = Playwright.create()) {
            // Launch Chromium in headless mode
            Browser browser = playwright.chromium().launch();
            Page page = browser.newPage();
            // Navigate and scrape product titles and prices
            page.navigate("https://cpit252.gitlab.io/html/lab-8/");
            List<String>  products = page.locator("h2.title").allInnerTexts();
            List<String>  prices = page.locator("p.price").allInnerTexts();
            for (int i=0; i< products.size(); i++) {
                if(products.get(i).indexOf(keyword) > -1) {
                    return products.get(i) + " " + prices.get(i);
                }
            }
            return null;
        }
    }
    public static void main(String[] args) {
        Observer fbObserver = new FacebookObserver("CPIT_252_price_watcher");
        Observer emailObserver = new EmailObserver("email@example.com");
        Observer waObserver = new WhatsappObserver("9660000000000");

        Subject s = new MessageSubject();

        s.subscribe(fbObserver);
        s.subscribe(emailObserver);
        // Get the price of a product (e.g., MacBook)
        String message = searchForProduct("MacBook");
        s.notifyUpdate(message);

        System.out.println("\nChange observers: unsubscribed email and subscribed Whatsapp\n");
        s.unsubscribe(emailObserver);
        s.subscribe(waObserver);

        s.notifyUpdate(message);
    }
}

{{< / highlight >}}

**Questions:**

1. Complete the implementation of the observer design pattern as shown in the code above?
2. Explain how the observer design pattern simplified the process of adding a new notification service in this implementation?
3. [Optional] Store the prices in a database or a cache such as [Ehcache](https://www.ehcache.org/).


## Deliverables and Submission

{{< githubclassroom "IT1 =#" "IT2 =#" "IT3 =#" "DAR =https://classroom.github.com/a/fY1lflJS" "IS1 =https://classroom.github.com/a/a-sr9MOS" >}}


#### Extra Task [Optional]
If you are done with this activity, you may enable a continuos integration tool such as [CircleCI ↗](https://circleci.com/) to automatically run your JUnit test upon code changes. You may also add more unit tests to increase [code coverage](https://cpit251.github.io/notes/unit-testing/). Please embed the badge that shows the status of your build and test (passing/failing) as well as the coverage percentage into your README file (e.g., 
<img src="https://raw.githubusercontent.com/travis-ci/travis-api/master/public/images/result/passing.png" alt="passing status image">
and <img src="https://raw.githubusercontent.com/travis-ci/travis-api/master/public/images/result/failing.png" alt="failing status image">). Please be sure to fork the repository or push to a remote repository under your own account, so you can enable the integration of CI tools in your own account.

You may refer to the lecture notes from the prerequisite course CPIT-251 on [Continuous Integration (CI) and adding a code coverage badge](https://cpit251.github.io/notes/continuous-integration/).

