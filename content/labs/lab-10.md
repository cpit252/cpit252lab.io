---
title: "Lab 10: Strategy Design Pattern"
date: 2022-04-15T13:34:56+03:00
draft: false
---



> The goal of this lab is to use the strategy design pattern to solve a real-world problem.


Design patterns are proven solutions to solve recurring design problems to design flexible and reusable object-oriented software. That is it, objects that are easier to implement, change, test and reuse. Behavioral pat­terns deal with the communication between objects and structuring the responsibilities between objects to keep­ them extensible, flex­i­ble and efficient.

The **strategy design pattern** (a.k.a. policy pattern) is one of the twenty-three well-known Gang of Four (GoF) design patterns. It is classified under the category of behavioral patterns, and it allows us to select an algorithm at runtime. Instead of implementing a single algorithm and using it, the strategy pattern allows us to select one from family of algorithms. It defers the decision about which algorithm to use until runtime allowing the algorithm to vary independently from clients that use it.

In this lab, we will work on creating a system that needs to support both multiple payment methods (e.g., Mada debit card, Visa credit card, PayPal, Google Pay, Apple Pay, etc.) and multiple receipt and invoice generation methods (paper, pdf, email, etc.)
The system will allow clients to decide which payment method and invoice method to choose at runtime.

#### Payment Methods:
<div class="lab-10-cards">

![Mada](/images/labs/lab-10/Mada.png)
![Visa](/images/labs/lab-10/Visa.png)
![PayPal](/images/labs/lab-10/PayPal.png)
![Apple Pay](/images/labs/lab-10/ApplePay.png)
![Google Pay](/images/labs/lab-10/GooglePay.png)

</div>

#### Invoice Methods:

<div class="lab-10-cards">

![Paper](/images/labs/lab-10/receipt.png)
![Email](/images/labs/lab-10/Email.png)
![PDF](/images/labs/lab-10/pdf.png)

</div>

## Video

{{< youtube id="Luaej4byyGI?rel=0" title="Lab 10: The strategy design pattern" >}}


## Objectives
In this lab you will
1. understand a real-world scenario and choose when to apply the appropriate design pattern.
2. design and implement the strategy design pattern.
3. learn how to send an email from your application in Java.
4. learn how to generate a PDF file from your application in Java.


### Requirement and Tools
- Java [JDK 15 or above.](https://www.oracle.com/java/technologies/downloads/)
  - This code uses [**Text block literals**](https://docs.oracle.com/en/java/javase/15/text-blocks/index.html), which are supported in Java SE 15 and later.
- A free account on [resend.com](https://resend.com).
- An IDE (e.g., [Apache NetBeans](https://netbeans.apache.org/), [Eclipse](https://www.eclipse.org/) or [IntelliJ IDEA](https://www.jetbrains.com/idea/)).
- If you do not like to use an IDE, you may use any text editor (e.g., [VS Code](https://code.visualstudio.com/), [jEdit](http://www.jedit.org/), etc.) and the _Javac_ compiler.
- [Apache Maven](https://maven.apache.org/) is a build automation tool to build projects and manage their dependencies.
- [Resend's Java SDK](https://github.com/resendlabs/resend-java).
- [Apache PDFBox - A Java PDF Library](https://pdfbox.apache.org/)  is an open source Java library for generating and working with PDF documents.


### Getting Started
If your instructor is using GitHub classroom, you will need to accept the assignment using [the link below](#deliverables-and-submission), clone the template repository, and import it as a project into your IDE.
If your instructor is not using GitHub classroom, clone and import the template project at [https://github.com/cpit252/lab-10 ↗](https://github.com/cpit252/lab-10).


#### Create an account on Resend
You also need to create an email account to send the receipt as an email message. We're going to use a developer friendly email service called [Resend](https://resend.com). Please sign up with your GitHub, generate an API key and save it.



#### Store your Resend's API key in the environment variables

> You should avoid storing your secrets (e.g., API keys, passwords, etc.) in the source code as hardcoded values. Instead, you should use environment variables. This lab makes use of two environment variables: `email` and `key`. Create and set the values of these two environment variables.  If you do not know how to set environment variables, please refer to the lecture note on **Environment Variables** under [Miscellaneous](/miscellaneous/#environment-variables).



### Problem Statement
A developer is working on an app that accepts a payment. The payment can be made in a number of payment methods (e.g., Mada debit card, Visa credit card, PayPal, Google Pay, Apple Pay, etc.). The app also supports generating payment receipts or invoices in multiple methods (paper, pdf, email, etc.).

The developer needs to add these two features (payment processing and receipt generation) in such a way that adding more payment and receipt delivery options won't require major refactoring of the code base. She wanted to have the user select a payment or receipt option at runtime in a more extensible, flex­i­ble, and efficient fashion. She thought of using the *Strategy* design pattern to make her payment and receipt implementations as loosely coupled as possible.

Below is an example of an email receipt generated by this program:

![Email](/images/labs/lab-10/Email-receipt.png)


**Questions:**

1. Complete the implementation of the strategy design pattern as shown in the code above?
2. Explain how the strategy design pattern simplified the process of adding a new payment and receipt delivery options?

## Deliverables and Submission

{{< githubclassroom "IT1 =#" "IT2 =#" "IT3 =#" "DAR =https://classroom.github.com/a/8Lisd-Ir" "IS1 =https://classroom.github.com/a/x7N67nIA" >}}

#### Extra Task [Optional]
If you are done with this activity, you may enable a continuos integration tool such as [CircleCI ↗](https://circleci.com/) to automatically run your JUnit test upon code changes. You may also add more unit tests to increase [code coverage](https://cpit251.github.io/notes/unit-testing/). Please embed the badge that shows the status of your build and test (passing/failing) as well as the coverage percentage into your README file (e.g., 
<img src="https://raw.githubusercontent.com/travis-ci/travis-api/master/public/images/result/passing.png" alt="passing status image">
and <img src="https://raw.githubusercontent.com/travis-ci/travis-api/master/public/images/result/failing.png" alt="failing status image">). Please be sure to fork the repository or push to a remote repository under your own account, so you can enable the integration of CI tools in your own account.

You may refer to the lecture notes from the prerequisite course CPIT-251 on [Continuous Integration (CI) and adding a code coverage badge](https://cpit251.github.io/notes/continuous-integration/).

