---
title: "Singleton Design Pattern"
date: 2022-03-23T09:35:55+03:00
toc: true
draft: false
---

## Slides

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSu2cj7fsDAAlDiWIb575x7GZKJ8Q9wtiIIUJooOGSjUvKCDGZpuSLtJbjYaIISKQ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Video

{{< youtube id="gHIpRpmtDlg?rel=0" title="The Singleton Design Pattern" >}}


## Code

__DBConnection.java__

{{< highlight java "linenos=table,hl_lines=9 11-16, linenostart=1" >}}

public class DBConnection{
  private static DBConnection uniqueInstance;

  // properties or instance variables here
  private int portNumber;
  private String hostName;
  // TODO: Add getters and setters

  private DBConnection(){}

  public static DBConnection getInstance(){
    if (uniqueInstance == null){
      uniqueInstance = new DBConnection();
    }
    return uniqueInstance;
  }
}

{{< / highlight >}}

__Main.java__

{{< highlight java "linenos=table,hl_lines=4 6, linenostart=1" >}}


public class Main{
  public static void main(String[] args){
    // create a single instance
    DBConnection instance1 = DBConnection.getInstance();
    // create another instance to show that it's exactly the same single instance
    DBConnection instance2 = DBConnection.getInstance();
    // Compare object identity to determine whether these two objects share and 
    // reference the same object (same memory address)
    if (instance1 == instance2) {
      System.out.println("ONE single instance of the class was created.");
    }
    else {
      System.err.println("Error: Multiple instances were created!");
    }
  }
}
{{< / highlight >}}


{{< gitlabLink "https://gitlab.com/cpit252/design-patterns/-/tree/main/01-singleton" >}}


## Usage of the singleton pattern in a real world scenario

The example below creates a singleton class that manages a database connection object for a PostgreSQL database. This example stores a To-do list in the database.

To run this code example, you will need to install and run PostgreSQL, and create a database named `tododb`

__DBConnection.java__
{{< highlight java "linenos=table,hl_lines=12 14 16 29-37,linenostart=1" >}}
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection{
  private final String url;
  private final int port;
  private final String dbName;
  private Connection connection;

  private static DBConnection instance;

  private DBConnection() throws SQLException {
    this.dbName = "tododb";
    this.port = 5432;
    this.url = "jdbc:postgresql://localhost:" + Integer.toString(this.port) + "/" + this.dbName;
    Properties props = new Properties();
    props.setProperty("user","postgres");
    props.setProperty("password","secret");
    props.setProperty("ssl","false");
    this.connection = DriverManager.getConnection(url, props);
  }

  public Connection getConnection() {
    return this.connection;
  }

  public static DBConnection getInstance() throws SQLException {
    if(instance == null){
      instance = new DBConnection();
    }
    else if (instance.getConnection().isClosed()) {
      instance = new DBConnection();
    }
    return instance;
  }
}
{{< / highlight >}}


__Task.java__
{{< highlight java "linenos=table,linenostart=1" >}}
import java.sql.*;

public class Task {
    private String name;
    private boolean isComplete;

    public Task(String name, boolean isComplete){
        this.name = name;
        this.isComplete = isComplete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public void insertTask(){
        try {
            Connection dbConnection = DBConnection.getInstance().getConnection();
            Statement stmt = dbConnection.createStatement();
            PreparedStatement insertStmt =
                    dbConnection.prepareStatement("INSERT INTO todo (task, status) VALUES (?, ?);");
            insertStmt.setString(1, this.name);
            insertStmt.setInt(2, (this.isComplete ? 1: 0));
            int rows = insertStmt.executeUpdate();
            System.out.println("Rows affected: " + rows);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void retrieveTasks(){
        try {
            Connection dbConnection = DBConnection.getInstance().getConnection();
            Statement stmt = dbConnection.createStatement();
            String query = "SELECT id, task, status FROM todo";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                //Display values
                String row = "ID: " + rs.getInt("id") +
                        " Task: " + rs.getString("task") +
                        " Status: " + rs.getInt("status") + "\n";
                System.out.print(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    // public void updateTask(){
    //
    //}

    public String toString(){
        return "Task: " + this.name + "\nStatus: " + (this.isComplete ? "1": "0");
    }
}

{{< / highlight >}}

__App.java__
{{< highlight java "linenos=table,linenostart=1" >}}
import java.io.PrintStream;
import java.sql.SQLException;

public class App {
    private static PrintStream out;

    public static void main(String[] args) {

        try {
            DBConnection db1 = DBConnection.getInstance();
            DBConnection db2 = DBConnection.getInstance();
            if (db1 == db2) {
                System.out.println("It's a singleton");
            } else {
                System.err.println("Error: Two different objects");
            }

            // Insert
            Task t = new Task("Do the laundry.", false);
            t.insertTask();
            // Retrieve all tasks
            t.retrieveTasks();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
{{< / highlight >}}
{{< gitlabLink "https://gitlab.com/cpit252/singletondbconnection" >}}


