---
title: "Builder"
date: 2023-04-09T10:34:10+03:00
draft: false
---



## Building an Ice Cream Example

__IceCream.java__
{{< highlight java "linenos=table, linenostart=1" >}}
public class IceCream {

    // Required IceCream fields
    private String type; // cup or cone
    private String size; // regular, medium, large
    private String flavor; // chocolate, strawberry, etc.

    // optional fields
    private String[] toppings;
    private String sauce;

    private IceCream(Builder builder) {
        this.type = builder.type;
        this.size = builder.size;
        this.flavor = builder.flavor;
        this.toppings = builder.toppings;
        this.sauce = builder.sauce;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Ice cream:\n " +
                this.type + " \t" + this.size + "\t" + this.flavor);
        if (this.sauce != "") {
            sb.append("\nSauce:\t" + sauce);
        }
        if (this.toppings.length > 0) {
            sb.append("\nToppings:");
            for (String t : toppings) {
                sb.append("\t" + t);
            }
        }
        return sb.toString();
    }

    public static class Builder {
        // Required builder fields
        private String type; // cup or cone
        private String flavor;
        private String[] toppings;

        // Optional builder fields
        private String sauce;
        private String size; // regular, medium, large

        public Builder(String type, String size, String flavor) {
            this.type = type;
            this.size = size;
            this.flavor = flavor;
        }

        public Builder withToppings(String[] toppings) {
            this.toppings = toppings;
            return this;
        }

        public Builder withSauce(String sauce) {
            this.sauce = sauce;
            return this;
        }

        public IceCream build() {
            return new IceCream(this);
        }

    }

}

{{< / highlight >}}

__Demo.java__
{{< highlight java "linenos=table, linenostart=1" >}}
public class Demo{
    public static void main(String[] args) {
        IceCream chocolateIcecream = 
        new IceCream.Builder("cone", "small", "chocolate")
        .withSauce("Caramel")
        .withToppings(new String[]{"Chocolate chip cookie", "Oreo crumbs", "M&M's"})
        .build();
        System.out.println(chocolateIcecream);
    }
}

{{< / highlight >}}

---
## Building Virtual Machine Example
__Builder.java__

{{< highlight java "linenos=table,hl_lines=27-38 79 116 121 126 131 136 141 146 151-153, linenostart=1" >}}

import java.util.Arrays;

public class VirtualMachine {

    // 10 required fields: CloudProvider, Region, AvailabilityZone, ImageId, InstanceClass, vpc, Subnet, SecurityGroup, and PublicKey, and publicIP.
    private String cloudProvider;
    private String region;
    private String availabilityZone;
    private String imageId;
    private String instanceClass;
    private String vpc;
    private String subnet;
    private String securityGroup;
    private String publicKey;
    private String publicIP;

    // 7 optional fields: Ports, PrivateIP, PlacementGroup, Architecture, Hypervisor, Storage, and Tags.
    private String[] ports;
    private String privateIP;
    private String placementGroup;
    private String architecture;
    private String hypervisor;
    private String storage;
    private String[] tags;

    // This creates a VirtualMachine instance with all required fields
    private VirtualMachine(VMBuilder builder) {
        this.cloudProvider = builder.cloudProvider;
        this.region = builder.region;
        this.availabilityZone = builder.availabilityZone;
        this.imageId = builder.imageId;
        this.instanceClass = builder.instanceClass;
        this.vpc = builder.vpc;
        this.subnet = builder.subnet;
        this.securityGroup = builder.securityGroup;
        this.publicKey = builder.publicKey;
        this.publicIP = builder.publicIP;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VirtualMachine{" +
                "cloudProvider='" + cloudProvider + '\'' +
                ", region='" + region + '\'' +
                ", availabilityZone='" + availabilityZone + '\'' +
                ", imageId='" + imageId + '\'' +
                ", instanceClass='" + instanceClass + '\'' +
                ", vpc='" + vpc + '\'' +
                ", subnet='" + subnet + '\'' +
                ", securityGroup='" + securityGroup + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", publicIP='" + publicIP + '\'');
        if (ports != null) {
            sb.append(", ports=" + Arrays.toString(ports));
        }
        if (privateIP != null) {
            sb.append(", privateIP='" + privateIP + '\'');
        }
        if (placementGroup != null) {
            sb.append(", placementGroup='" + placementGroup + '\'');
        }
        if (architecture != null) {
            sb.append(", architecture='" + architecture + '\'');
        }
        if (hypervisor != null) {
            sb.append(", hypervisor='" + hypervisor + '\'');
        }
        if (storage != null) {
            sb.append(", storage='" + storage + '\'');
        }
        if (tags != null) {
            sb.append(", tags=" + Arrays.toString(tags));
        }
        sb.append("}");
        return sb.toString();
    }

    public static class VMBuilder {
        // 10 required fields: CloudProvider, Region, AvailabilityZone, ImageId, InstanceClass, vpc, Subnet, SecurityGroup, and PublicKey, and publicIP.
        private String cloudProvider;
        private String region;
        private String availabilityZone;
        private String imageId;
        private String instanceClass;
        private String vpc;
        private String subnet;
        private String securityGroup;
        private String publicKey;
        private String publicIP;

        // 7 optional fields: Ports, PrivateIP, PlacementGroup, Architecture, Hypervisor, Storage, and Tags.
        private String[] ports;
        private String privateIP;
        private String placementGroup;
        private String architecture;
        private String hypervisor;
        private String storage;
        private String[] tags;

        public VMBuilder(String cloudProvider, String region, String availabilityZone, String imageId,
                         String instanceClass, String vpc, String subnet, String securityGroup,
                         String publicKey, String publicIP) {
            this.cloudProvider = cloudProvider;
            this.region = region;
            this.availabilityZone = availabilityZone;
            this.imageId = imageId;
            this.instanceClass = instanceClass;
            this.vpc = vpc;
            this.subnet = subnet;
            this.securityGroup = securityGroup;
            this.publicKey = publicKey;
            this.publicIP = publicIP;
        }

        public VMBuilder withPorts(String[] ports) {
            this.ports = ports;
            return this;
        }

        public VMBuilder withPrivateIP(String privateIP) {
            this.privateIP = privateIP;
            return this;
        }

        public VMBuilder withPlacementGroup(String placementGroup) {
            this.placementGroup = placementGroup;
            return this;
        }

        public VMBuilder withArchitecture(String architecture) {
            this.architecture = architecture;
            return this;
        }

        public VMBuilder withHypervisor(String hypervisor) {
            this.hypervisor = hypervisor;
            return this;
        }

        public VMBuilder withStorage(String storage) {
            this.storage = storage;
            return this;
        }

        public VMBuilder withTags(String[] tags) {
            this.tags = tags;
            return this;
        }

        public VirtualMachine build(){
            return new VirtualMachine(this);
        }
    }
}

{{< / highlight >}}

__App.java__

{{< highlight java "linenos=table,hl_lines=7-10 15-20, linenostart=1" >}}
public class App 
{
    public static void main( String[] args )
    {
        // Create a VM on AWS, us-east, az-1, ubuntu20.04-098im, t2.micro, myvpc, mysubnet,
        // sg1, my-vm-pub-key.
        VirtualMachine awsVM = new VirtualMachine.VMBuilder("AWS", "us-east", "az-1",
                "ubuntu20.04-098im", "t2.micro", "myvpc",
                "mysubnet", "sg1", "my-vm-key.pub", "53.24.12.15")
                .build();
        System.out.println("launching a VM on AWS \n" + awsVM);

        // Create a VM on Azure, us-west, az-1, windows-server.2022-5483im, a.series, myvpc, mysubnet,
        // sg1, my-vm-pub-key, ports [3389, 80, 443], and tags ["cpit252", "lab"]
        VirtualMachine azureVM = new VirtualMachine.VMBuilder("Azure", "us-west", "az-1",
                "ubuntu20.04-098im", "a.series", "myvpc",
                "mysubnet", "sg1", "my-vm-pub-key", "53.24.12.15")
                .withPorts(new String[]{"3389", "80", "443"})
                .withTags(new String[]{"cpit252", "lab"})
                .build();

        System.out.println("launching a VM on Azure \n" + azureVM);
    }
}
{{< / highlight >}}
