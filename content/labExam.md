---
title: "Lab Exam Overview"
date: 2023-11-26T17:10:20+03:00
draft: false
---


## Lab Exam Overview

The lab exam is worth 10% of your final grade. It is a closed-book exam, and you must work independently. Any form of collaboration or discussion with other students or use of unauthorized tools is strictly prohibited. This includes:
 - No use of generative AI tools (e.g., ChatGPT, Bard, CoPilot, etc.)
 - No use of communication tools to check exam answers with others

Any violation of these rules is considered cheating and will result in disciplinary action and a zero score for the exam. If you have any questions about the exam, please direct them to your lab instructor.
Remember to maintain the highest standards of academic integrity throughout the exam. 

### Exam Duration

The exam duration is 80 minutes and will be closed for submission automatically at the end of the time. It's your responsibility to monitor time during the exam.

### Exam Requirements

To complete the exam, you may use any PC connected to the Internet and complete the exam on GitHub. Alternatively, you may bring your own laptop if you prefer to.

#### GitHub with VS Code Option

- Any PC connected to the Internet with the following:
  - Web Browser
  - GitHub account. 

More on this option next.

#### Personal Laptop Option
- You may bring your own laptop with the following tools installed:
  - An IDE (e.g., IntelliJ IDEA)
  - Git + GitHub account
  - Command Line (e.g., Terminal, PowerShell, etc.) to clone the project and push to GitHub

### Project Structure

You must keep the source code as a Maven Java project. Changing it to a non-Maven project will render your solution ineligible for grading.

### Unit Tests

You should not modify the provided unit tests in any way.


### VS Code on GitHub Instructions

If you choose to use VS Code on GitHub:

1. Accept the assignment (lab exam).
2. Click on the link to your repository on GitHub.com
3. Press the key `>` on your keyboard.
4. Complete your work and click on the left side icon for source control, type your commit message and click commit.

![](/images/labs/practice-exam/github-vs-code-1.png)

![](/images/labs/practice-exam/github-vs-code-2.png)

### Exam Submission

Similar to all previous lab activities, you will submit your work on GitHub, which will be used to grade your code automatically. Please complete the practice question below prior to the exam and attempt to submit your answer on the tool you wish to use either VS Code on GitHub or your own laptop.

Please adhere to these instructions carefully to ensure a fair and honest exam experience. Good luck!

### Practice Sample Exam Question


#### Question

A Java application uses a fixed thread pool to execute some tasks by a fixed number of worker threads. Creating threads is always expensive, regardless of the platform your code is running on. This app uses a thread pool, but it ended up creating the thread pool two times and may repeat that multiple times. Fix the current implementation of the `MyThreadPool.java` class, so it creates a thread pool ONLY once and clients can use it multiple times throughout the life of the application.

> NOTE: Do not make changes to any class other than the `MyThreadPool` class.


Please accept the exam using the link for your class section:
{{< githubclassroom "IT2 =https://classroom.github.com/a/UOGmNRy0" "AA1 =https://classroom.github.com/a/fsLjmXGn" "ADB =https://classroom.github.com/a/YTT2Szdu" "ADD =https://classroom.github.com/a/at9S2z4T" >}}