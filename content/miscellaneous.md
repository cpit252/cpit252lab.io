---
title: "Miscellaneous"
date: 2022-02-05T15:42:15+03:00
toc: true
draft: false
---

## Writing Unit Test in Java

This guide will help you configure the unit testing framework, JUnit, to write unit tests in Apache NetBeans, IntelliJ IDEA, and from the command line.

### Unit Testing in the Apache NetBeans IDE

1. Create a Java application project with Maven 

    ![create a project with Maven](/images/miscellaneous/netbeans-junit/step-01.png)
2. Complete the project details (name, location, group id, version and package) and click Finish

    ![create a project with Maven - project details](/images/miscellaneous/netbeans-junit/step-02.png)
3. Right Click on the "source packages" and select New -> "Java Class..."

    ![create a new Java class](/images/miscellaneous/netbeans-junit/step-03.png)
4. Give the new class a name, select a package and click "Finish".

    ![add unit test in NetBeans](/images/miscellaneous/netbeans-junit/step-04.png)
5. Right click on the Java class that you want to write unit test for and select "Tools" -> "Create/Update Tests"

    ![add unit test in NetBeans with Junit](/images/miscellaneous/netbeans-junit/step-05.png)
6. Select "Junit" as the testing framework and depending on your project, you may need to uncheck the auto generated test code stubs.

    ![add unit test in NetBeans with Junit](/images/miscellaneous/netbeans-junit/step-06.png)
7. This should result in a new Java test class with the name `ClassNameTest.java`

    ![create a project with Maven](/images/miscellaneous/netbeans-junit/step-07.png)
8. Write the Junit assertions:

    ![writing JUnit tests](/images/miscellaneous/netbeans-junit/step-08.png)
9. To run your test, select the main menu item "Run" and "Test Project". This may take a while if it's the first time to download the required "Jar" files into your local repository.


**Note:** If you are using an older version of NetBeans (e.g., version 8.2), then right click on the project and select "New" -> "JUnit Test".

***

### Unit Testing in the IntelliJ IDEA IDE

1. Create a new Project and select "Maven" from the left side and check "Create from archtype" and select `org.apache.maven.archetypes:maven-archetype-quickstart`. Then, click "Next"

    ![create a project with Maven](/images/miscellaneous/idea-junit/step-01.png)
2. Complete the project details (name, location, groupId, artifactId and version) and click "Next".

    ![create a project with Maven - Project Details](/images/miscellaneous/idea-junit/step-02.png)
3. Review the project details and click "Finish"

    ![create a project with Maven - Confirm Project Details](/images/miscellaneous/idea-junit/step-03.png)
4. Place the cursor on the class you want to write unit test for, and press `ALT` and `ENTER` on Windows (`⌥ ⏎` on macOS), and select *Create Test*

    ![create a project with Maven - Confirm Project Details](/images/miscellaneous/idea-junit/step-04.png)
5. Select the unit testing framework and click "OK".

    ![Write unit test in IDEA](/images/miscellaneous/idea-junit/step-05.png)
6. Write the unit test assertions.

    ![Write unit test in IDEA](/images/miscellaneous/idea-junit/step-06.png)

7. To run a particular unit test class, right click on the test file and select "Run".

***

### Unit Testing from the Command Line

1. Download and install [Apache Maven](https://maven.apache.org/).
2. Add maven (mvn) the PATH environment variable in your operating system.
   - In Windows: 
      - Open the Start Search, type in "env", and choose “Edit the system environment variables”
      - Click the "Environment Variables…" button.
      - Under the "System Variables" section, select "Path" in the first column, and click edit.
      - Click "New" and add the path to the "mvn" excutable file.
   - In macOS and Linux:
     - Add `mvn` to the Path variable under `~/.bash_profile` if you use the *Bash* shell or `~/.zprofile` if you use the *Zsh* shell.
3. Create a new Java project
   ```bash
   mvn archetype:generate -DgroupId=edu.kau.fcit.cpit \
   -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart \
   -DarchetypeVersion=1.4 -DinteractiveMode=false
   ```
4. This should result in a project with the following structure:
   ```bash
       ├── pom.xml
		└── src
		    ├── main
		    │   └── java
		    │       └── edu
		    │           └── kau
		    │               └── fcit
		    │                   └── cpit
		    │                       └── App.java
		    └── test
		        └── java
		            └── edu
		                └── kau
		                    └── fcit
		                        └── cpit
		                            └── AppTest.java
   ```
5. The unit test file is located under `src/test/java/edu/kau/fcit/cpit/AppTest.java`. To run the test, use:
    ```bash
       mvn test
    ```
6. To change the Junit version, edit the dependency section in the `pom.xml` file and re-run the test using `mvn test`
    ```xml
       <dependencies>
          <dependency>
             <groupId>junit</groupId>
             <artifactId>junit</artifactId>
             <version>4.13.2</version>
             <scope>test</scope>
          </dependency>
      </dependencies>

    ```

***

## Environment Variables

Environment variables are variables whose values are set outside of the program. Instead of hardcoded program variables, environment variables are used to store configuration options (e.g., database port number, log path, etc.) and user sensitive data (e.g., database username and password). They provide better security, custom configuration and improve program portability. Environment variables are defined and stored at the system-wide level. Below is how you can set environment variables in the NetBeans IDE, IntelliJ IDEA IDE, and from the command line:

### Setting Environment Variables in the NetBeans IDE version 12.X
1. Right click on **project**, select **Properties** and then click on **Actions**.

    ![environment variables](/images/miscellaneous/environment-variables/env-variables-netbeans-1.jpeg)
2. From the list of Actions select **Run Project**
    ![environment variables](/images/miscellaneous/environment-variables/env-variables-netbeans-2.jpeg)
3. Click on **Add** button and click on **New Environment Variable**.
    ![environment variables](/images/miscellaneous/environment-variables/env-variables-netbeans-3.jpeg)
4. Type in the environment variable `Env.<variable>=<Value>` as shown below:
    ![environment variables](/images/miscellaneous/environment-variables/env-variables-netbeans-4.png)
5. Repeat step 3 to add more environment variables.
### Setting Environment Variables in the IntelliJ IDEA IDE
1. From the main menu, select **Run | Edit Configurations** or choose **Edit Configurations** from the run/debug configurations selector on the toolbar.

    ![environment variables](/images/miscellaneous/environment-variables/idea-env-vars-1.png)
2. Type the variable name and value: `<name>=<value>`. If you add several variables, they should be separated with semicolons.
    ![environment variables](/images/miscellaneous/environment-variables/idea-env-vars-2.png)

### Setting Environment Variables in the Shell/Terminal

```shell
export variableName=Value
export username=khalid
```


***

## Adding Continuous Integration (CI)
Travis CI is a continuous integration service used to build and test projects hosted on open source code repositories such as GitHub and GitLab. To Get started, see [Travis CI Tutorial](https://docs.travis-ci.com/user/tutorial/) and [Building and testing Java with Maven with GitHub Actions](https://docs.github.com/en/actions/automating-builds-and-tests/building-and-testing-java-with-maven)
